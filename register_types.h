#ifndef PAVO_REGISTER_TYPES_H
#define PAVO_REGISTER_TYPES_H

#include "pavo_reader.h"

void register_pavo_types();
void unregister_pavo_types();

#endif
