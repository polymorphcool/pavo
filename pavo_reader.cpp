/*
 * pavo_reader.cpp
 *
 *  Created on: Jul 6, 2022
 *      Author: frankiezafe
 */

#include "pavo_reader.h"

PavoReader::PavoReader() :
_pavo_ip(""),
_pavo_port(2368),
_reception_port(2368),
_autostart(true),
_started(false) {
	set_process_internal(true); // flag object to receive internal process notifications
}

PavoReader::~PavoReader() {
	stop();
}

void PavoReader::_thread_func(void *s) {

	PavoReader* pv = (PavoReader*) s;

	pv->reception_mutex.lock();
	bool keep_on = pv->_started;
	pv->reception_mutex.unlock();

	while( keep_on ) {
		uint64_t start = _OS::get_singleton()->get_ticks_msec();
		pv->udp_update();
		uint64_t delta = _OS::get_singleton()->get_ticks_msec() - start;
		// maximum 100Hz!
		if ( delta < 10 ) {
			_OS::get_singleton()->delay_msec(10 - delta);
		}
		pv->reception_mutex.lock();
		keep_on = pv->_started;
		pv->reception_mutex.unlock();
	}
}

Error PavoReader::udp_update() {

	Error err;
	err = server_udp.poll();

	if ( err == OK && server_udp.is_connection_available() ) {

		Ref<PacketPeerUDP> peer = server_udp.take_connection();
		String pip = peer->get_packet_address();
		int pport = peer->get_packet_port();
		std::wstring ws = pip.c_str();
		std::string ipstring(ws.begin(), ws.end());
		if ( pip == _pavo_ip && pport == _pavo_port ) {
			std::cout << "valid PAVO IP " << ipstring << ":" << pport << std::endl;
			peers.push_back( peer );
		}

	}


	const uint8_t *packet_buffer;
	int packet_size = 0;

	for ( int i = 0; i < peers.size(); ++i ) {
		Ref<PacketPeerUDP>& peer = peers[i];
		int wait_count = peer->get_available_packet_count();
		if ( wait_count == 0 ) { continue; }

//		String pip = peer->get_packet_address();
//		int pport = peer->get_packet_port();
//		std::wstring ws = pip.c_str();
//		std::string ipstring(ws.begin(), ws.end());

		// decompressing
		for ( int p = 0; p < wait_count; ++p ) {
			err = peer->get_packet( &packet_buffer, packet_size );
			if ( err == OK ) {
				if ( packet_size == pavo_data_packet_t_size ) {
					pavo_data_packet_t pdp;
					std::memcpy( &pdp, packet_buffer, pavo_data_packet_t_size );
					load_packet( pdp );
				}
			}
		}

	}

	// updating back frame
	frame_copy_mutex.lock();
	back_frame = reception_frame;
	frame_copy_mutex.unlock();

	return OK;

}

void PavoReader::load_packet( const pavo_data_packet_t& pdp ) {

	for ( int i = 0; i < FIRING_PER_PKT; ++i ) {

		const pavo_firing_data_t& pfd = pdp.firingData[i];

		pavo_point3d_t& pp3d = reception_frame[pfd.rotationalPosition];

		float avrg_dist = -1;
		float avrg_intensity = -1;
		float div = 0;

		for ( int j = 0; j < DATA_PER_FIRING; ++j ) {
			const pavo_laser_return_t& plr = pfd.laserReturns[j];
			if ( plr.distance == pavo_distance_max ) {
				continue;
			}
			avrg_dist += plr.distance;
			avrg_intensity += plr.intensity;
			div += 1;
		}

		if ( div == 0 ) {
			pp3d.position *= 0;
			pp3d.intensity *= 0;
			pp3d.valid = false;
		} else {
			float rad = (pfd.rotationalPosition%36000) * Math_TAU / 36000.0f;
			pp3d.position.x = Math::cos( rad ) * avrg_dist;
			pp3d.position.y = 0;
			pp3d.position.z = Math::sin( rad ) * avrg_dist;
			pp3d.intensity = avrg_intensity;
			pp3d.valid = true;
		}

	}

}

Error PavoReader::init(const int& port) {
	if ( _reception_port == port ) {
		return OK;
	}
	stop();
	_reception_port = port;
	return start();

}

Error PavoReader::start() {

	if (_started) {
		return OK;
	}

	reception_mutex.lock();
	_started = false;
	reception_mutex.unlock();

	Error err;
	err = server_udp.listen( _reception_port );
	if ( err != OK ) {
		return err;
	}

	reception_mutex.lock();
	_started = err == OK;
	reception_mutex.unlock();

	if ( _started ) {
		reception_thread.start(_thread_func, this);
	}

	return err;

}

void PavoReader::stop() {

	reception_mutex.lock();
	_started = false;
	reception_mutex.unlock();

	reception_thread.wait_to_finish();

	server_udp.stop();
	for ( int i = 0; i < peers.size(); ++i ) {
		peers[i]->close();
	}
	peers.clear();

}

void PavoReader::set_pavo_ip(const String& ip) {
	_pavo_ip = ip;
	_pavo_ip_address = IP_Address( ip );
}

void PavoReader::set_pavo_port(const int& port) {
	_pavo_port = port;
}

void PavoReader::set_reception_port(const int& port) {
	init( port );
}

void PavoReader::set_autostart(const bool& autostart) {
	_autostart = autostart;
}

Vector<Vector3> PavoReader::get_points() {
	Vector<Vector3> out;
	for (int i = 0; i < front_frame.point_num; ++i) {
		pavo_point3d_t& pp3d = front_frame.points[i];
		if ( pp3d.valid ) {
			out.push_back( pp3d.position );
		}
	}
	return out;
}

void PavoReader::_notification(int p_what) {

	switch (p_what) {

		case MainLoop::NOTIFICATION_WM_QUIT_REQUEST:
			std::cout << "PavoReader::_notification, stopping reception" << std::endl;
			stop();
			break;

		case NOTIFICATION_READY: {
			if (_autostart) {
				std::cout << "PavoReader::_notification, starting reception" << std::endl;
				start();
			}
		}
			break;

		case NOTIFICATION_INTERNAL_PROCESS: {
			if ( _started ) {
				// updating back frame
				frame_copy_mutex.lock();
				front_frame = back_frame;
				frame_copy_mutex.unlock();
			}
		}
			break;

	}

}

void PavoReader::_bind_methods() {

	ClassDB::bind_method(D_METHOD("start"), &PavoReader::start);
	ClassDB::bind_method(D_METHOD("stop"), &PavoReader::stop);

	ClassDB::bind_method(D_METHOD("set_pavo_ip", "pavo_ip"), &PavoReader::set_pavo_ip);
	ClassDB::bind_method(D_METHOD("set_pavo_port", "pavo_port"), &PavoReader::set_pavo_port);
	ClassDB::bind_method(D_METHOD("set_reception_port", "reception_port"), &PavoReader::set_reception_port);
	ClassDB::bind_method(D_METHOD("set_autostart", "autostart"), &PavoReader::set_autostart);

	ClassDB::bind_method(D_METHOD("get_pavo_ip"), &PavoReader::get_pavo_ip);
	ClassDB::bind_method(D_METHOD("get_pavo_port"), &PavoReader::get_pavo_port);
	ClassDB::bind_method(D_METHOD("get_reception_port"), &PavoReader::get_reception_port);
	ClassDB::bind_method(D_METHOD("is_autostart"), &PavoReader::is_autostart);
	ClassDB::bind_method(D_METHOD("is_started"), &PavoReader::is_started);

	ClassDB::bind_method(D_METHOD("get_points"), &PavoReader::get_points);

	ADD_GROUP("Network", "");
	ADD_PROPERTY(PropertyInfo(Variant::STRING, "pavo_ip", PROPERTY_HINT_LENGTH, "7"),
			"set_pavo_ip", "get_pavo_ip");
	ADD_PROPERTY(
			PropertyInfo(Variant::INT, "pavo_port", PROPERTY_HINT_RANGE,
					"1,99999,1"), "set_pavo_port", "get_pavo_port");
	ADD_PROPERTY(
			PropertyInfo(Variant::INT, "reception_port", PROPERTY_HINT_RANGE,
					"1,99999,1"), "set_reception_port", "get_reception_port");

	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "autostart"), "set_autostart",
			"is_autostart");

}
