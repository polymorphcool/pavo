# PAVO

This is a Godot game engine module to receive [PAVO lidar](http://www.siminics.com/Goods/Detail?id=4) data directly from the sensor.

## Installation

### Godot engine
First, you will need the Godot engine sources. Head over to [Godot compilation instructions](http://docs.godotengine.org/en/latest/development/compiling/) to get Godot building on your system (make sure it can build with `use_llvm=yes`).

Checkout tag [3.4](https://github.com/godotengine/godot/tree/3.4)

### Dependencies

#### linux

If you wants to use clang, install it with:

`sudo apt install clang`

#### git

If you are not familiar with git, make sure it is installed:

`sudo apt install git`

Generate SSH key by following this [tutorial](https://www.siteground.com/kb/generate_ssh_key_in_linux/) and add it to your [gitlab](https://docs.gitlab.com/ee/user/ssh.html) account - HTTPS links are not working with submodules!

Once done, use these 2 commands to get the repo up & running:

`git clone git@gitlab.com:polymorphcool/pavo.git pavo`
