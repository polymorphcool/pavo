/*
 * pavo_reader.h
 *
 *  Created on: Jul 6, 2022
 *      Author: frankiezafe
 */

#ifndef MODULES_PAVO_PAVO_READER_H_
#define MODULES_PAVO_PAVO_READER_H_

#include <cstring>
#include <iostream>

#include "core/engine.h"
#include "scene/main/node.h"
#include "core/io/udp_server.h"
#include "core/io/packet_peer_udp.h"
#include "core/bind/core_bind.h"

// data structure from pavo SDK

#if defined(_WIN32)
#pragma pack(1)
#define __DATA_ALIGN__
#else
#define __DATA_ALIGN__ __attribute__((packed))
#endif

#define DATA_PER_FIRING (2)
#define FIRING_PER_PKT (12)

typedef struct pavo_laser_return
{
  uint16_t distance;
  uint8_t intensity;
} __DATA_ALIGN__ pavo_laser_return_t;

typedef struct pavo_firing_data
{
  uint16_t blockIdentifier;
  uint16_t rotationalPosition;
  pavo_laser_return_t laserReturns[DATA_PER_FIRING];
} __DATA_ALIGN__ pavo_firing_data_t;

typedef struct pavo_data_packet
{
  pavo_firing_data_t firingData[FIRING_PER_PKT];
  uint32_t timestamp;
  uint8_t blank1;
  uint8_t blank2;
}__DATA_ALIGN__ pavo_data_packet_t;

#define PAVO_DISTANCE_ZERO 0
#if PAVO_DISTANCE_ZERO
const int pavo_distance_max = 50000; //100m
#else
const int pavo_distance_max = 0; //0m
#endif

// custom constant

#define PAVO_LOCAL_IP "127.0.0.1"

typedef struct pavo_point3d {
	uint16_t rotationalPosition;
	Vector3 position;
	float intensity;
	bool valid;
} pavo_point3d_t;

typedef struct pavo_frame {
	uint32_t timestamp;
	uint32_t point_num;
	List<pavo_point3d> points;
	pavo_frame() : point_num(0) {}
	pavo_point3d& operator [] ( uint16_t r ) {
		for ( int i = 0; i < point_num; ++i ) {
			if ( points[i].rotationalPosition ==  r) {
				return points[i];
			}
		}
		pavo_point3d newp;
		newp.rotationalPosition = r;
		newp.intensity = -1;
		newp.valid = false;
		points.push_back( newp );
		point_num += 1;
		return points[ point_num-1 ];
	}
	void operator = ( const pavo_frame& src ) {
		timestamp = src.timestamp;
		point_num = src.point_num;
		points = src.points;
	}
} pavo_frame_t;

class PavoReader: public Node {
GDCLASS(PavoReader, Node);

public:
	PavoReader();
	virtual ~PavoReader();

	Error start();
	void stop();
	static void _thread_func(void *s);

	// setters
	void set_pavo_ip(const String& ip);
	void set_pavo_port(const int& port);
	void set_reception_port(const int& port);
	void set_autostart(const bool& autostart);

	// getters
	_FORCE_INLINE_ const String& get_pavo_ip() const {
		return _pavo_ip;
	}
	_FORCE_INLINE_ const int& get_pavo_port() const {
		return _pavo_port;
	}
	_FORCE_INLINE_ const int& get_reception_port() const {
		return _reception_port;
	}
	_FORCE_INLINE_ const bool& is_autostart() const {
		return _autostart;
	}
	_FORCE_INLINE_ const bool& is_started() const {
		return _started;
	}

	Vector<Vector3> get_points();

protected:

	void _notification(int p_what);
	static void _bind_methods();

	Error init(const int& port);
	Error udp_update();
	void load_packet( const pavo_data_packet_t& );

	enum {
		PACKET_BUFFER_SIZE = 65536
	};

	String _pavo_ip;
	IP_Address _pavo_ip_address;
	int _pavo_port;
	int _reception_port;
	bool _autostart;
	bool _started;

	Mutex reception_mutex;
	Thread reception_thread;

	UDPServer server_udp;
	List<Ref<PacketPeerUDP>> peers;

	const int pavo_data_packet_t_size = sizeof(pavo_data_packet_t);

	pavo_frame_t reception_frame;
	pavo_frame_t back_frame;
	pavo_frame_t front_frame;
	Mutex frame_copy_mutex;

};

#endif /* MODULES_PAVO_PAVO_READER_H_ */
